//
//  DeliverViewController.swift
//  Yommy
//
//  Created by Jair on 7/22/17.
//  Copyright © 2017 Edgar Alejandro Rodriguez Zavala. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import AddressBookUI
let manager = CLLocationManager()
var userLatitude = 0.00000000
var userLongitude = 0.0000000
var adressText:String!
class DeliverViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var addressInput: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        let currentLocation = manager.location
        userLongitude = (currentLocation?.coordinate.longitude)!
        userLatitude = (currentLocation?.coordinate.latitude)!
        //getAdress
        let location = CLLocation(latitude: CLLocationDegrees(userLatitude), longitude: CLLocationDegrees(userLongitude))
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print(error ?? "error")
                return
            }
            else if (placemarks?.count)! > 0 {
                let pm = placemarks![0]
                let address = ABCreateStringWithAddressDictionary(pm.addressDictionary!, false)
                adressText = address
                self.addressInput.text = address
            }
        })
    }
}
