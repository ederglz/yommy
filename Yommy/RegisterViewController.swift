//
//  RegisterViewController.swift
//  Yommy
//
//  Created by Jair on 7/22/17.
//  Copyright © 2017 Edgar Alejandro Rodriguez Zavala. All rights reserved.
//

import Foundation
import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!

    @IBOutlet weak var mailField: UITextField!

    @IBOutlet weak var yearsField: UITextField!
    
    @IBOutlet weak var preferencesField: UITextField!

    @IBOutlet weak var femenino: UIButton!

    @IBOutlet weak var masculino: UIButton!
    @IBOutlet weak var selectMasculino: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    var mail = ""
    var name = ""
    var years = ""
    var preferences = ""
    
    @IBAction func femenino1(_ sender: Any) {
    }
    

    @IBAction func selectFemenino(_ sender: Any) {
        femenino.backgroundColor = hexStringToUIColor(hex: "F8C745")
        masculino.backgroundColor = UIColor.white
    }

    
    
    @IBAction func selectMasculino(_ sender: Any) {
        femenino.backgroundColor = UIColor.white
        masculino.backgroundColor = hexStringToUIColor(hex: "F8C745")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    
    
    @IBAction func textChanged(_ sender: Any) {
        if((sender as AnyObject).isEqual(nameField)){
            name = nameField.text!
        }
        else if((sender as AnyObject).isEqual(yearsField)){
            years = yearsField.text!
        }
        else if((sender as AnyObject).isEqual(mailField)){
            mail = mailField.text!
        }
        else if((sender as AnyObject).isEqual(preferencesField)){
            preferences = preferencesField.text!
        }
        if !name.isEmpty && !years.isEmpty && !mail.isEmpty && !preferences.isEmpty {
            nextButton.isEnabled = true
            nextButton.backgroundColor = UIColor(red:0.94, green:0.78, blue:0.40, alpha:1.0)
        } else {
            nextButton.isEnabled = false
        }
    }
    
    @IBAction func sendData(_ sender: UIButton) {
        if nextButton.isEnabled {
          let register: String = "https://841ba814.ngrok.io/mommy/insert"
            guard let url = URL(string: register)
                else {
                    print("Error: cannot create URL")
                    return
            }
            var urlRequest = URLRequest(url: url)
            let parameters = ["mail": mail ,"name": name ,"years": years ,"preferences": preferences ]
            urlRequest.httpMethod = "POST"
            let session = URLSession.shared
            let task = session.dataTask(with: urlRequest){
                (data, response, error) in
                
                guard error == nil else {
                    print("error calling GET on /POC/")
                    print(error!)
                    return
                }
                
                
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                } catch let error {
                    print(error.localizedDescription)
                }

            }
            task.resume()
        }
    }
}
